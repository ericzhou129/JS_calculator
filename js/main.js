// Global variables
var number1 = 0;
var number2 = 0;
var saved_number = 0;
var current_number = 0;
var clicked_number_str = ""
var num_count = 1;
var operator_str = "";
var operator_count = 0;


// functions add, subtract, divide and multiply take 2 numbers and apply the mathematical operation to them. 
function add(num1, num2) {
    var ans = num1 + num2;
    return ans
}

function subtract(num1, num2) {
    var ans = num1 - num2;
    return ans

}

function divide(num1, num2) {
    var ans = num1/num2;
    return ans
}

function multiply(num1, num2) {
    var ans = num1*num2;
    return ans
}

// function to parse the number string into a number
function num_convert(num_str) {
    var num = parseFloat(num_str);
    if (isNaN(num)) {
        alert('Error: not a number!');
        return;
    }
    return num;
}

// function which sets the num_count variable
function num_count_set(num) {
    if (num == 2) {
        num_count = 1;             
    }   
    else if (num_count == 1){
        num_count += 1;
    }
    else (
        console.log("error at num_count_set function")
    )
    console.log("num_count is: "+ num_count);
    return;
}

function assign_num(num){
    saved_number = current_number;
    current_number = num;
    return;
}

function print_num(num) {
    $('#screen h2').text(Number((num).toFixed(10)));
}

function equals(){
        // TODO: Scope of answer variable?
        var ans = 0;

        // clear the number string
        clicked_number_str = '';
        
        // carry out the operation
        switch (operator_str) {
            case operator_str = '+':
                ans = add(saved_number, current_number);
                break;
            case operator_str = '-':
                ans = subtract(saved_number, current_number);
                break;
            case operator_str = '/':
                ans = divide(saved_number, current_number);
                break;
            case operator_str = '*':
                ans = multiply(saved_number, current_number);
                break;
            default:
                ans = saved_number;
                console.log('saved_number: ' + saved_number);
                break;
        }
        
        // set number variable
        num_count_set(num_count);

        // Output onto screen
        print_num(ans);
        return ans;
}


function main() {    
 
    //check input size and other errors
        //TODO: check overflow

    // ***CLICK ANY NUMBER BUTTON***
    $('.num_button').on('click', function(){
        var number = 0;

        // prevent input of more than 1 '.'
        if ($(this).text() == '.' && clicked_number_str.includes('.')) {
            return
        }

        // concatenate the text of the clicked number onto the number string
        clicked_number_str += $(this).text();
        
        // convert to number
        assign_num(num_convert(clicked_number_str));

        $('#screen h2').text(current_number);

        
        console.log('saved_number: '+ saved_number);
        console.log('current_number: '+ current_number);

        // output onto screen
    });


    // ***CLICK ANY OPERATOR BUTTON***
    $('.operator_button').on('click', function() {
        // clear the number string
        clicked_number_str = '';

        // if error -> error message
        
        operator_count += 1;

        // if operator button is clicked a second time before equals button
        if (operator_count == 2) {
            // execute equals and assign the number to number1 or number2
            assign_num(equals());
            // reset operator count
            operator_count = 1;
        }

        //set operator
        operator_str = $(this).text();
        console.log(operator_str);

        // set numcount to the next
        num_count_set(num_count);

    });


    // ***CLICK THE EQUALS BUTTON***
    $('#equals_button').on('click', function() {
        var answer = equals();
        console.log('answer: ' + answer);
    });

    // ***CLICK THE AC BUTTON***
    $('#ac_button').on('click', function() {
        saved_number = 0;
        current_number = 0;
        clicked_number_str = '';
        console.log("ac clicked");
        console.log(saved_number + ',' + current_number);
        $('#screen h2').text('---');

    });


    // ***CLICK THE CE BUTTON***
    $('#ce_button').on('click', function() {
        current_number = 0;
        clicked_number_str = '';
        console.log("ce clicked");
        console.log(saved_number + ',' + current_number);
        $('#screen h2').text('---');

    });

}
$(document).ready(main);



